const express = require("express");
const app = express();
const bcrypt = require("bcrypt");
const cors = require("cors");

app.use(express.json());

const users = [];
app.use(cors());
app.get("/users", (req, res) => {
  res.json(users);
});

app.post("/users", async (req, res) => {
  try {
    const hashedPassword = await bcrypt.hash(req.body.password, 10);
    const user = {
      name: req.body.name,
      email: req.body.email,
      password: hashedPassword,
    };
    users.push(user);
    res.status(201).send();
  } catch {
    res.status(500).send();
  }
});

app.post("/users/login", async (req, res) => {
  const name = req.body.name;
  const email = req.body.email;
  const password = req.body.password;
  if (name === "test" && password === "test" && email === "email@email.com")
    res.status(200).json({
      message: "Logged in successfully",
      token: "secret token",
    });
  res.status(400).json({
    message: "wrong name or password or email",
  });
});

app.listen(1379);
