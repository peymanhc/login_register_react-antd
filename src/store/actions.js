import * as actionTypes from "./actionTypes";

export const authSuccess = (token, email, name, password) => {
  return {
    type: actionTypes.AUTH_LOGIN,
    token: token,
    name: name,
    email: email,
    password: password,
  };
};
export const authLogout = () => {
  return {
    type: actionTypes.LOG_OUT,
  };
};
