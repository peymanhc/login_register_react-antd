import * as actionTypes from "./actionTypes";

const initialState = {
  token: null,
  email: null,
  name : null,
  password : null
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTH_LOGIN:
      return {
        ...state,
        email: action.email,
        name : action.name,
        token: action.token,
        password: action.password
      };
    case actionTypes.LOG_OUT:
      return {
        ...state,
        token: null,
        email: null,
        name: null,
        password: null
      };
    default:
      return state;
  }
};
export default reducer;
