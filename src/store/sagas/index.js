import { takeEvery, all } from "redux-saga/effects";
import * as actionTypes from "../actionTypes";
import { logoutSaga, authUserSaga } from "./saga";

export function* watchAuth() {
  yield all([
    takeEvery(actionTypes.LOG_OUT, logoutSaga),
    takeEvery(actionTypes.AUTH_LOGIN, authUserSaga),
  ]);
}
