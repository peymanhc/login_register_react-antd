import { call } from "redux-saga/effects";
import axios from "axios";
import Swal from "sweetalert2";

export function* authUserSaga(action) {
  const authData = {
    token: action.token,
    name: action.name,
    email: action.email,
    password: action.password,
  };
  let url = "http://localhost:1379/users/login";
  try {
    const res = yield axios.post(url, authData);
    console.log("res", res.data.message);
    yield localStorage.setItem("token", res.data.token);
    yield Swal.fire({
      icon: "success",
      title: "Success",
      text: res.data.message,
    });
    setTimeout(() => {
      window.location.reload();
    }, 500);
  } catch (error) {
    yield Swal.fire({
      icon: "error",
      title: "Failed",
      text: error.message,
    });
    console.log(error);
  }
}
export function* logoutSaga(action) {
  yield call([localStorage, "removeItem"], "token");
  yield Swal.fire({
    icon: "warning",
    title: "LogingOut ...",
  });
  setTimeout(() => {
    window.location.reload();
  }, 500);
}
