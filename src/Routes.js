import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Login from "./pages/Login/login";
import Home from "./pages/home/Home";
function App() {
  const token = localStorage.getItem("token");
  return (
    <BrowserRouter>
      <Switch>
        {token !== null ? (
          <Route component={Home} />
        ) : (
          <Route component={Login} />
        )}
      </Switch>
    </BrowserRouter>
  );
}

export default App;
