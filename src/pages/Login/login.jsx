import React from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from 'react-redux'
import { authSuccess } from '../../store/actions';
import "./styles.css";
import axios from 'axios'

function Login(props) {

  const { register, handleSubmit } = useForm();
  const dispatch = useDispatch();

  const SubmitBtn = (data) => {

    axios.post("http://localhost:1379/users/login", {
      name: data.name,
      email: data.email,
      password: data.password,
      token: data.token
    })
    dispatch(authSuccess(data.token, data.email, data.name, data.password))
  };

  return (
    <div className="wrapper">
      <div className="final__register-block">
        <h3 className="final__register-title">Create an account</h3>
        <p>
          Please don't say any thing ...
          <br /> Just Login
        </p>
        <form onSubmit={handleSubmit(SubmitBtn)}>
          <input
            ref={register({ required: true })}
            id="name"
            name="name"
            type="name"
            placeholder="Enter your name"
          />
          <input
            ref={register({ required: true, minLength: 5 })}
            id="email"
            name="email"
            type="email"
            placeholder="Enter your email"
          />
          <input
            ref={register({ required: true })}
            id="password"
            name="password"
            type="password"
            placeholder="Enter your password"
          />
          <input id="SubmitBtn" type="submit" value="Create my account" />
        </form>
      </div>
      <div>
        <p><strong>name :</strong> test</p>
        <p><strong>email :</strong> email@email.com</p>
        <p><strong>password :</strong> test</p>
      </div>
    </div>
  );
}
export default Login;
