import React from 'react';

import 'antd/dist/antd.css';
import { Card, Layout, Menu, } from 'antd';
import Title from 'antd/lib/typography/Title';
import SubMenu from 'antd/lib/menu/SubMenu';
import Meta from 'antd/lib/card/Meta';
import { useDispatch  } from 'react-redux';
import { authLogout } from '../../store/actions';
const { Header, Sider, Content } = Layout;

function Home() {
    const dispatch = useDispatch();
    const Logout = (data) => {
        dispatch(authLogout('token'))
    }
    return (
        <div>
            <Layout>
                <Header style={{ padding: 10 }}>
                    <Title style={{ padding: "10px", color: 'white' }} level={4}>Peyman Hadavi
                    <button id="btn"
                            className="active"
                            onClick={Logout}
                            style={{ float: "right", background: "red", cursor: "pointer", padding: "10px", marginTop: "-10px" }} >LogOut</button>
                    </Title>
                </Header>
                <Layout>
                    <Sider>
                        <Menu
                            defaultSelectedKeys={['Dashboard']}
                            mode="inline">
                            <SubMenu title={<span> <span>Panel</span> </span>}>
                                <Menu.Item key='email' >Peymanhc@gmail.com</Menu.Item>
                                <Menu.Item key='Home'><a href="/" >Home</a></Menu.Item>
                                <Menu.Item key='Settings'> Settings</Menu.Item>
                            </SubMenu>
                        </Menu>
                    </Sider>
                    <Layout>
                        <Content style={{
                            padding: '30px',
                            border: "2px solid black",
                            textAlign: "center",
                            background: 'white',
                            minHeight: "90vh",
                        }}>
                            <Card style={{ border: "2px solid black", }} >
                                <Meta id="Myname" title={`Hello test `} />
                            </Card>
                        </Content>
                    </Layout>
                </Layout>
            </Layout>
        </div>
    );
}

export default Home;