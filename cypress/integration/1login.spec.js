describe("Login >", () => {
  it("load Page", () => {
    cy.visit("http://localhost:3000/");
  });
  it("Name", () => {
    cy.get("#name").type("test");
  });
  it("Email", () => {
    cy.get("[type='email']").type("email@email.com");
  });
  it("Password", () => {
    cy.get("#password").type("test");
  });
  it("SubmitBtn", () => {
    cy.get("#SubmitBtn").click();
  });
});
